def test_index(client):
    res = client.get("/")
    assert res.content == b"Hello, world!"
