# Docker Django Boilerplate

My default setup for a Django site with Docker.

## Versions

* Base: Alpine
* Python: 3.7
* Django: 2.0

## Usage

Start with:

    docker-compose up

See available `make` commands:

    make

## TODO

- Use https://www.caktusgroup.com/blog/2017/03/14/production-ready-dockerfile-your-python-django-app/

- Remove test/dev requirements from production version.

- Add a second Dockerfile that inherits the default one but removes all frontend
  code (except the built bundle of course).

- Use immutable.js.

- Use redux.

## License

ISC
