FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=config.settings
ENV UWSGI_WSGI_FILE=config/wsgi.py
ENV UWSGI_HTTP=:8000
ENV UWSGI_MASTER=1
ENV UWSGI_WORKERS=2
ENV UWSGI_THREADS=8
ENV UWSGI_UID=1000
ENV UWSGI_GID=1000
ENV UWSGI_LAZY_APPS=1
ENV UWSGI_WSGI_ENV_BEHAVIOR=holy
ADD requirements.freeze.txt /app/
RUN set -ex && \
    apk add --no-cache --virtual .build-deps \
        gcc \
        libc-dev \
        linux-headers \
        make \
        musl-dev \
        pcre-dev \
        postgresql-dev \
        && \
    pip install --no-cache-dir -r /app/requirements.freeze.txt && \
    apk add --virtual .python-rundeps libcrypto1.0 libpq libssl1.0 musl pcre zlib && \
    apk del .build-deps && \
    apk add --no-cache nodejs yarn && \
    addgroup -g 1000 -S user && \
    adduser -g 1000 -u 1000 -S user -G user
WORKDIR /app
ADD ./ /app/
RUN chown -R user:user /app
USER user
RUN cd frontend && \
    yarn && \
    ../scripts/build-static && \
    rm -fr node_modules ~/.yarn ~/.yarnrc ~/.cache
EXPOSE 8000
EXPOSE 3000
WORKDIR /app/src
CMD ["uwsgi", "--http-auto-chunked", "--http-keepalive"]
