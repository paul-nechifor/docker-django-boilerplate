import './App.css';
import React, { Component } from 'react';
import logo from './logo.svg';
import { translate, Trans } from "react-i18next";

class App extends Component {
  render() {
    const { t, i18n } = this.props;
    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">{t("welcomeToReact")}</h1>
          <button onClick={() => changeLanguage("en")}>en</button>
          <button onClick={() => changeLanguage("ro")}>ro</button>
        </header>
        <Trans i18nKey="noChangeSee">
          No <strong>change</strong>. See{" "}
          <a href="" onClick={e => { e.preventDefault(); }} >this</a>.
        </Trans>
      </div>
    );
  }
}

export default translate("translations")(App);
