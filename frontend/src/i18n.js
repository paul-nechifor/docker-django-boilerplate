import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

i18n.use(LanguageDetector).init({
  fallbackLng: "en",
  debug: true, // Turn off in production.
  ns: ["translations"],
  defaultNS: "translations",
  keySeparator: false,
  interpolation: { escapeValue: false, formatSeparator: "," },
  react: { wait: true },
  resources: {
    en: {
      translations: {
        "noChangeSee": "No <1>change</1>. See <4>this</4>.",
        "welcomeToReact": "Welcome to React"
      }
    },
    ro: {
      translations: {
        "noChangeSee": "Nicio <1>schimbare</1>. Vezi <4>asta</4>.",
        "welcomeToReact": "Bine ai venit la React"
      }
    }
  }
});

export default i18n;
