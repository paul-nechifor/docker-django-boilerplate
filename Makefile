.DEFAULT_GOAL := help

APP_RUN := docker-compose exec app
APP_RUN_ROOT := docker-compose exec --user=root app

define PRINT_HELP_PYSCRIPT
from __future__ import print_function
import re, sys

def print_formatted(target, hlp):
	print(("\033[95m%%-%ss\033[0m %%s" % (15,)) % (target, hlp))

if len(sys.argv) == 1:
	for line in sys.stdin:
		match = re.match(r'^([a-zA-Z_-]+)\s*:.*?## (.*)$$', line)
		if match:
			target, help = match.groups()
			print_formatted(target, help)
else:
	print_formatted(*sys.argv[1:])
endef
export PRINT_HELP_PYSCRIPT
help:
	@echo "Commands:"
	@echo ""
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

test: lint isort black coverage bandit ## run tests and all related checks
.PHONY: test

fast:  ## run the unit tests fast
	$(APP_RUN) pytest
.PHONY: fast

lint:  ## run the linting checks
	$(APP_RUN) python -W ignore -m flake8
.PHONY: lint

isort:  ## run the sorted imports checker
	$(APP_RUN) isort -rc -c -df .
.PHONY: isort

black:  ## check if code matches black autoformatting
	$(APP_RUN) black -q -l 79 --diff --check .
.PHONY: black

coverage:  ## run tests and check code coverage
	$(APP_RUN) pytest --cov=.
.PHONY: coverage

bandit:  ## run bandit checks
	$(APP_RUN) bandit -i -l -s B101 -r .
.PHONY: bandit

auto-fix: isort-fix black-fix  ## autofix import sorting and code formatting
.PHONY: auto-fix

isort-fix:  ## autofix the import order
	$(APP_RUN) isort -rc -y .
.PHONY: isort-fix

black-fix:  ## autofix code formatting with black
	$(APP_RUN) black -l 79 .
.PHONY: black-fix

freeze:  ## freeze the list of requirements
	$(APP_RUN_ROOT) sh -c 'pip freeze | sort > /app/requirements.freeze.txt'
.PHONY: freeze

watch:  ## rerun tests when the code changes
	$(APP_RUN) ptw
.PHONY: watch

sh:  ## open a user shell
	@$(APP_RUN) sh
.PHONY: sh

root:  ## open a root shell
	@$(APP_RUN_ROOT) sh
.PHONY: root

upgrade-js: ## upgrade JavaScript packages
	$(APP_RUN_ROOT) sh -c 'cd ../frontend; yarn upgrade'
.PHONY: upgrade-js

clean:  ## perform all of the clean steps
	find . -name '*.pyc' -exec rm -fr {} +
	find . -name '__pycache__' -exec rm -fr {} +
	rm -fr .coverage.*
.PHONY: clean
